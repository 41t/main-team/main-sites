import { EArea } from './enums/EArea.ts';

export const linkRegExps = {
  [EArea.ORG]: RegExp(/^(.+\.)?41t\.org$/),
  [EArea.DEV]: RegExp(/^(.+\.)?41t\.dev$/),
  [EArea.LABS]: RegExp(/^(.+\.)?labs\.41t\.dev$/),
  [EArea.DESIGN]: RegExp(/^(.+\.)?design\.41t\.dev$/),
  [EArea.SPACE]: RegExp(/^(.+\.)?41t\.space$/)
}
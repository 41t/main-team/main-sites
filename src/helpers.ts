import { EArea } from './enums/EArea.ts';
import { linkRegExps } from './linkRegExps.ts';

export function getArea(hostname: string): EArea {
  let area = EArea.ORG;

  Object.entries(linkRegExps).forEach(([key, reg]) => {
    if (hostname.match(reg)) area = key as EArea;
  });

  return area;
}
import { writable } from 'svelte/store';
import { EArea } from './enums/EArea.ts';

export const areaStore = writable(import.meta.env.PUBLIC_AREA as EArea ?? EArea.ORG);
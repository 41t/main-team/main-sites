export enum EArea {
  ORG = "org",
  DEV = "dev",
  DESIGN = "design",
  LABS = "labs",
  SPACE = "space",
}
